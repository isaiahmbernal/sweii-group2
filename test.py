import os
import sys
import unittest
from models import app, db, Companies, Industries, Sectors

class DBTestCases(unittest.TestCase):
    # ---------
    # insertion
    # ---------
    
    def test_source_insert_1(self):

        with app.app_context():
            company_1 = Companies(ticker='TMUS', name='T-Mobile US Inc', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.QDmT8QyI5_Lg4IlAIIp8lgHaGM%26pid%3DApi&f=1&ipt=525a0fbf6e19e0330099a6bb352687bf45d1fcf8c4c853d4ab88c258098ffb36&ipo=images', price=142.45, marketCap=173000000000, volume=9164000)
            db.session.add(company_1)
            db.session.commit()


            company = db.session.query(Companies).filter_by(ticker='TMUS').one()
            self.assertEqual(str(company.ticker), 'TMUS')

            db.session.query(Companies).filter_by(ticker='TMUS').delete()
            db.session.commit()
    
    def test_source_insert_2(self):

        with app.app_context():
            company_2 = Companies(ticker='RAD', name='Rite Aid Corporation', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flogos-download.com%2Fwp-content%2Fuploads%2F2017%2F07%2FRite_Aid_logo.png&f=1&nofb=1&ipt=f3a08e0cecec148d5d176dff6e8ad51d9b9d0993c314f677abaec0c85f6b1e92&ipo=images', price=2.68, marketCap=151483000, volume=6424000)
            db.session.add(company_2)
            db.session.commit()


            company = db.session.query(Companies).filter_by(ticker='RAD').one()
            self.assertEqual(str(company.ticker), 'RAD')

            db.session.query(Companies).filter_by(ticker='RAD').delete()
            db.session.commit()

    def test_source_insert_3(self):

        with app.app_context():

            company_3 = Companies(ticker='NFLX', name='Netflix, Inc', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.mmzLP_kqdkMjz8ezo0pAzAHaD4%26pid%3DApi&f=1&ipt=819406ac2515de6fb4bf6e1d9e058db878a3905dc67d8da8ed65c2dddabc2ae3&ipo=images', price=303.50, marketCap=135163000000, volume=6919000)
            db.session.add(company_3)
            db.session.commit()


            company = db.session.query(Companies).filter_by(ticker='NFLX').one()
            self.assertEqual(str(company.ticker), 'NFLX')

            db.session.query(Companies).filter_by(ticker='NFLX').delete()
            db.session.commit()

    def test_source_insert_4(self):

        with app.app_context():

            company_4 = Companies(ticker='NKE', name='Nike, Inc.', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.WlTthXGmL7Ax0lYId5fCGwHaFS%26pid%3DApi&f=1&ipt=f8f1c0bee165bbc8a67fe75ca6ee130850e35676a1cce0261ba2eb9ea2417075&ipo=images', price=120.39, marketCap=186685000000, volume=12869000)
            db.session.add(company_4)
            db.session.commit()


            company = db.session.query(Companies).filter_by(ticker='NKE').one()
            self.assertEqual(str(company.ticker), 'NKE')

            db.session.query(Companies).filter_by(ticker='NKE').delete()
            db.session.commit()
    
    def test_source_insert_5(self):

        with app.app_context():

            industry_1 = Industries(name='Coal', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.3-0jMJlYUfEi6wgICow-UAHaHC%26pid%3DApi&f=1&ipt=3fc8f407d40770a25d818f371277a07d638cd0c6a3faf143df247bc1ba77bb5a&ipo=images', dividends=2.49, change1d=-.46, marketCap=34055000000, volume=2932000)
            db.session.add(industry_1)
            db.session.commit()


            industry = db.session.query(Industries).filter_by(name='Coal').one()
            self.assertEqual(str(industry.name), 'Coal')

            db.session.query(Industries).filter_by(name='Coal').delete()
            db.session.commit()

    def test_source_insert_6(self):

        with app.app_context():

            industry_2 = Industries(name='Oil Refining/Marketing', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.Qov_c-yvgq-slTQiXJ5jfQHaHa%26pid%3DApi&f=1&ipt=e4f114add7926511e29b934488e644d5ba8d5c92f52d33129084c3ca379972d1&ipo=images', dividends=4.49, change1d=-.71, marketCap=187222000000, volume=10550000)
            db.session.add(industry_2)
            db.session.commit()

            industry = db.session.query(Industries).filter_by(name='Oil Refining/Marketing').one()
            self.assertEqual(str(industry.name), 'Oil Refining/Marketing')

            db.session.query(Industries).filter_by(name='Oil Refining/Marketing').delete()
            db.session.commit()
    
    def test_source_insert_7(self):

        with app.app_context():

            industry_3 = Industries(name='Forest Products', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.wmf159Jf-u13_EaMPY5qbgHaE5%26pid%3DApi&f=1&ipt=e4c7ca663d04c7db110b3c5ca5e357db7fe0a62083b04e97251a9e222826c1c0&ipo=images', dividends=0.85, change1d=-1.60, marketCap=26914000000, volume=1251000)
            db.session.add(industry_3)
            db.session.commit()


            industry = db.session.query(Industries).filter_by(name='Forest Products').one()
            self.assertEqual(str(industry.name), 'Forest Products')

            db.session.query(Industries).filter_by(name='Forest Products').delete()
            db.session.commit()
    
    def test_source_insert_8(self):

        with app.app_context():

            sector_1 = Sectors(name='Energy Minerals', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.8EqzdH6BOmt8kU7RMSepWwHaHa%26pid%3DApi&f=1&ipt=a0715238d940de29f456680ed999922fab8782088aed61735b8044c10ace8d74&ipo=images', dividends=7.47, change1d=-1.37, marketCap=2441000000000, volume=18132000)
            db.session.add(sector_1)
            db.session.commit()


            sector = db.session.query(Sectors).filter_by(name='Energy Minerals').one()
            self.assertEqual(str(sector.name), 'Energy Minerals')

            db.session.query(Sectors).filter_by(name='Energy Minerals').delete()
            db.session.commit()

    def test_source_insert_9(self):

        with app.app_context():

            sector_2 = Sectors(name='Transportation', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.hr6flR9AmEUaFRJxFfahfgHaH1%26pid%3DApi&f=1&ipt=360d4e1cd1deaca767059e20d1d1be3d7bf77e4376e9ce7b8278f07f12d03155&ipo=images', dividends=2.05, change1d=-1.28, marketCap=1106000000000, volume=9956000)
            db.session.add(sector_2)
            db.session.commit()


            sector = db.session.query(Sectors).filter_by(name='Transportation').one()
            self.assertEqual(str(sector.name), 'Transportation')

            db.session.query(Sectors).filter_by(name='Transportation').delete()
            db.session.commit()

    def test_source_insert_10(self):

        with app.app_context():

            sector_3 = Sectors(name='Retail Trade', image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.sfl92DQNzcLBowJsbN8qEQHaFJ%26pid%3DApi&f=1&ipt=a567c7235bc97d551d733ee961907d9f31816cbd0994f22987bc62db6a3114f7&ipo=images', dividends=1.00, change1d=-.83, marketCap=3555000000000, volume=31830000)
            db.session.add(sector_3)
            db.session.commit()


            sector = db.session.query(Sectors).filter_by(name='Retail Trade').one()
            self.assertEqual(str(sector.name), 'Retail Trade')

            db.session.query(Sectors).filter_by(name='Retail Trade').delete()
            db.session.commit()


if __name__ == '__main__':
    unittest.main()