from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from cons import password
import os

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

''' Use variable password from separate py file'''
# urlVar = 'postgresql://postgres:' + password + '@localhost:5432/stonkr'

USER = "postgres"
PASSWORD = "asd123"
PUBLIC_IP_ADDRESS = "34.29.67.139"
DBNAME = "postgres"

URI_STRING = "postgresql://" + USER + ":" + PASSWORD + "@" + PUBLIC_IP_ADDRESS + "/" + DBNAME

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", URI_STRING)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True #to suppress a warning message
db = SQLAlchemy(app)


class Companies(db.Model):

    """ 
    Every column is a string due to symbols coming through from the external source
    Having the percentages and abbreviated large values makes things cleaner

    Ticker = ticker of the stock, what we use as the key

    Name = name of the stock, must be present

    Image = photo of the companies logo, must be present

    Price = price of the stock, must be present

    marketCap = value of the company, must be present

    volume = number of shares traded in one day, must be present

    sector_name = the overarching category of the company
    Use the foreign key of sectors.name from the sectors table

    industry_name = the subcategory of the company
    Use the foreign key of industries.name from the industries table
    """
    
    __tablename__ = 'companies'


    ticker = db.Column(db.String(1000), primary_key = True)
    name = db.Column(db.String(1000), nullable = False)
    image = db.Column(db.String(10000), nullable = False)
    price = db.Column(db.String(1000), nullable = False)
    marketCap = db.Column(db.String(1000), nullable = False)
    volume = db.Column(db.String(1000), nullable = False)
    sector_name = db.Column(db.String(1000), db.ForeignKey('sectors.name'))
    industry_name = db.Column(db.String(1000), db.ForeignKey('industries.name'))



class Industries(db.Model):

    """    
    name = name of the industry, must be present

    image = general photo of the industry logo, must be present

    dividends = percent of industry given to shareholders, must be present

    change1d = percent change of industry for one day, must be present

    marketCap = value of the industry, must be present

    volume = number of shares traded in this industry in one day, must be present

    sector_name = higher category of the industry, uses name from sectors table

    industry_companies = companies inside this industry, use backreference from companies (one (industry) to many (companies))
    """

    __tablename__ = 'industries'

    name = db.Column(db.String(1000), primary_key = True)
    image = db.Column(db.String(10000), nullable = False)
    dividends = db.Column(db.String(1000), nullable = False)
    change1d = db.Column(db.String(1000), nullable = False)
    marketCap = db.Column(db.String(1000), nullable = False)
    volume = db.Column(db.String(1000), nullable = False)
    sector_name = db.Column(db.String(1000), db.ForeignKey('sectors.name'))
    industry_companies = db.relationship('Companies', backref='industry_to_companies')


class Sectors(db.Model):

    """
    name = name of the sector, must be present

    image = general photo of the sector logo, must be present

    dividends = percent of sector given to shareholders, must be present

    change1d = percent change of sector for one day, must be present

    marketCap = value of the sector, must be present

    volume = number of shares traded in this sector in one day, must be present

    sector_companies = companies inside this sector, use backreference from companies (one (sector) to many (companies))

    sector_industries = industries inside this sector, use backreference from companies (one (sector) to many (industries))
    """

    __tablename__ = 'sectors'
    
    name = db.Column(db.String(1000), primary_key = True)    
    image = db.Column(db.String(10000), nullable = False)    
    dividends = db.Column(db.String(1000), nullable = False)
    change1d = db.Column(db.String(1000), nullable = False)
    marketCap = db.Column(db.String(1000), nullable = False)
    volume = db.Column(db.String(1000), nullable = False)
    sector_companies = db.relationship('Companies', backref='sector_to_companies')
    sector_industries = db.relationship('Industries', backref='sector_to_industries')


with app.app_context():
    #db.drop_all()
    db.create_all()
