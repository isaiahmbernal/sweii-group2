import json
import requests
from bs4 import BeautifulSoup
import urllib
from urllib.request import urlopen
import yfinance as yf


f = open('JSON_NASDAQ_Tickers.json')
data = json.load(f)

final_companies = {"Companies": []}
with open('JSON_Output_Companies.json', 'w') as fp:
    json.dump(final_companies, fp)

# For list of tickers
listOfTickers = []

for item in data:
    listOfTickers.append(item['ticker'])

def getHTMLdocument(url):
    response = requests.get(url)
    return response.text

for item in data:
    print(item)
    urlForScrape = f'https://www.tradingview.com/symbols/NASDAQ-{item["ticker"]}/'
    try:
        htmlWebsite = urlopen(urlForScrape)
        soup = BeautifulSoup(htmlWebsite, 'html.parser')
    except:
        continue

    # For logos
    images = soup.find_all('img', {"src":True})
    notFound = True
    for image in images:
        if 'logo' in image['src']:
            newDict = {"image": image['src']}
        break

    # For marketCap[0], Sector[5], Industry[6]
    alldivs = soup.find_all('div', {'class': "apply-overflow-tooltip value-bXapFXBA"})
    allValues = []
    for div in alldivs:
        value = div.text
        allValues.append(value)
    
    if len(allValues) < 7:
        continue

    newDict.update({"marketCap":str(allValues[0]), "sector": str(allValues[5]), "industry": str(allValues[6])})


    alldivs = soup.findAll('h1', {'class': "apply-overflow-tooltip title-_YFUN_8t"})
    otherValues = []
    for div in alldivs:
        value = div.text

    if "Common Stock" in item["name"]:
        newItem = item["name"].replace(" Common Stock","")

    newDict.update({"ticker": item["ticker"], "name": newItem})

    # msft = yf.Ticker("MSFT")
    # hist = msft.history(period="1mo")
    # list = []
    # for item in hist:
    #     print(item)
    # print(hist)
    # print(list)


    #get all stock info (slow)
    data = yf.download(item["ticker"], start="2023-03-20", end="2023-03-22")
    JSON_data = json.loads(data.to_json())
    for key in JSON_data["Close"]:
        stock_price = str("{:.2f}".format(JSON_data["Close"][key]))
    newDict.update({"price": stock_price})
    for key in JSON_data["Volume"]:
        stock_volume = str(JSON_data["Volume"][key])
    newDict.update({"volume": stock_volume})
    #print(newDict)

    f = open('JSON_Output_Companies.json')
    currentJSON = json.load(f)    
    currentJSON["Companies"].append(newDict)

    with open('JSON_Output_Companies.json', 'w') as fp:
        json.dump(currentJSON, fp)