import json

with open('copied_sectors.txt') as file:
    oddNumber = True
    index = 0
    list_of_sectors = []
    for line in file:
        if oddNumber:
            list_of_sectors.append([line.strip('\n')])
            oddNumber = False
        else:
            separated_words = line.split()
            for position in range(0, len(separated_words) - 1):
                if position == 1 or position == 5 or position == 6:
                    continue
                else:
                    list_of_sectors[index].append(
                    separated_words[position])
            oddNumber = True
            index += 1

for position in range(0, len(list_of_sectors)):
    current_item = list_of_sectors[position][3]
    new_item = current_item.replace('âˆ’', "")
    list_of_sectors[position][3] = new_item


final_dictionary = {"Sectors": []}
for item in list_of_sectors:
    new_object = {
        "name": item[0],
        "image": "",
        "dividends" : item[2],
        "change1d": item[3],
        "marketCap": item[1],
        "volume": item[4]
    }
    final_dictionary["Sectors"].append(new_object)

print(final_dictionary)

with open('JSON_Output_Sectors.json', 'w') as fp:
    json.dump(final_dictionary, fp)