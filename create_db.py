from models import app, db, Companies, Industries, Sectors
import json 

def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn


def create_sectors():

    with app.app_context():

        sectordata = load_json('JSON_Output_Sectors.json')

        for oneSector in sectordata['Sectors']:
            name = oneSector['name']
            image = oneSector['image']
            dividends = oneSector['dividends']
            change1d = oneSector['change1d']
            marketCap = oneSector['marketCap']
            volume = oneSector['volume']

            newSector = Sectors(name = name, image = image, dividends = dividends, change1d = change1d, marketCap = marketCap, volume = volume)
            
            # print(type(db.session.get(Sectors, name)))

            if (db.session.get(Sectors, name) == None):
                db.session.add(newSector)

            # try:
            #     db.session.add(newSector)
            # except:
            #     db.session.rollback()
            #     # raise
            # else:

        db.session.commit()

def create_industries():
    with app.app_context():

        industrydata = load_json('JSON_Output_Industries.json')

        for oneIndustry in industrydata['Industries']:
            name = oneIndustry['name']
            image = oneIndustry['image']
            dividends = oneIndustry['dividends']
            change1d = oneIndustry['change1d']
            marketCap = oneIndustry['marketCap']
            volume = oneIndustry['volume']
            sector_name = oneIndustry['sector']

            newIndustry = Industries(name = name, image = image, dividends = dividends, change1d = change1d, marketCap = marketCap, volume = volume, sector_name = sector_name)
            
            if (db.session.get(Industries, name) == None):
                db.session.add(newIndustry)

            # try:
            #     db.session.add(newIndustry)
            # except:
            #     db.session.rollback()
            #     # raise
                
        
        db.session.commit()

def create_companies():

    with app.app_context():

        companydata = load_json('JSON_Output_Companies.json')

        for oneCompany in companydata['Companies']:
            ticker = oneCompany['ticker']
            name = oneCompany['name']
            image = oneCompany['image']
            price = oneCompany['price']
            marketCap = oneCompany['marketCap']
            volume = oneCompany['volume']
            industry_name = oneCompany['industry']
            sector_name = oneCompany['sector']

            newCompany = Companies(ticker = ticker, name = name, image = image, price = price, marketCap = marketCap, volume = volume, industry_name = industry_name, sector_name = sector_name)

            if (db.session.get(Companies, ticker) == None):
                db.session.add(newCompany)

            # try:
            #     db.session.add(newCompany)
            # except:
            #     db.session.rollback()
            #     # raise
            
        db.session.commit()

create_sectors()
create_industries()    
create_companies()    