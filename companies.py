from flask import Blueprint, jsonify
from models import db, Companies


companies_blueprint = Blueprint("companies_blueprint", __name__,)   



@companies_blueprint.route('/')
def companies():
    companies = Companies.query.all()
    # print("Companies: ", companies)
    companies_list = []
    for company in companies:
        company_dict = make_dictionarycompany(company)
        companies_list.append(company_dict)
    return jsonify(companies_list)

@companies_blueprint.route('/<string:ticker>')
def company(ticker):
    company = Companies.query.get(ticker)
    return jsonify(make_dictionarycompany(company))


def make_dictionarycompany(company):
    company_dict = {
          'ticker': company.ticker, 
          'name': company.name,
          'image': company.image,
          'price': company.price,
          'marketCap': company.marketCap,
          'volume': company.volume,
          'industries': company.industry_name,
          'sectors': company.sector_name
        }
    return company_dict
