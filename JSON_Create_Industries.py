import json

with open('copied_industries.txt') as file:
    oddNumber = True
    index = 0
    list_of_industries = []
    for line in file:
        if oddNumber:
            list_of_industries.append([line.strip('\n')])
            oddNumber = False
        else:
            separated_words = line.split()
            if len(separated_words) == 8:
                for position in range(0, len(separated_words) - 1):
                    print(separated_words[position])
                    if position == 1 or position == 7:
                        continue
                    else:
                        list_of_industries[index].append(
                            separated_words[position])
            if len(separated_words) == 7:
                for position in range(0, len(separated_words) - 1):
                    if position == 1 or position == 6:
                        continue
                    else:
                        list_of_industries[index].append(
                            separated_words[position])
            oddNumber = True
            index += 1

print(list_of_industries, "bruh")
for position in range(0, len(list_of_industries)):
    if len(list_of_industries[position]) == 7:
        current_item = list_of_industries[position]
        first_word = current_item[5]
        second_word = current_item[6]
        full_word = first_word + ' ' + second_word
        new_item = current_item[0:5]
        new_item.append(full_word)
        list_of_industries[position] = new_item


for position in range(0, len(list_of_industries)):
    current_word = list_of_industries[position][3]
    new_word = current_word.replace('âˆ’', '')
    list_of_industries[position][3] = new_word

print(list_of_industries)

final_dictionary = {"Industries": []}
for item in list_of_industries:
    new_object = {
        "name": item[0],
        "dividends": item[2],
        "change1d": item[3],
        "marketCap": item[1],
        "volume": item[4],
        "sector": item[5],
        "image": ""
    }
    final_dictionary["Industries"].append(new_object)

print(final_dictionary)

with open('JSON_Output_Industries.json', 'w') as fp:
    json.dump(final_dictionary, fp)
