from flask import Blueprint, jsonify
from models import db, Industries


industries_blueprint = Blueprint("industries_blueprint", __name__,)   


@industries_blueprint.route('/')
def industries():
    industries = Industries.query.all()
    industries_list = []
    for industry in industries:
        industry_dict = make_dictionaryindustry(industry)
        industries_list.append(industry_dict)
    return jsonify(industries_list)

@industries_blueprint.route('/<slug>')
def industry(slug):
    print("Slug:", slug)
    if " - " in slug:
        slug = slug.replace(" - ", "/")
        print("New Slug:", slug)
    industry = Industries.query.get(slug)
    initial_JSON = make_dictionaryindustry(industry)
    companies = industry.industry_companies
    company_list = []

    for company in companies:
        company_list.append(company.name)
    
    initial_JSON.update({"companies": company_list})

    return jsonify(initial_JSON)


def make_dictionaryindustry(industry):
    industry_dict = {
        "name": industry.name,
        "image": industry.image,
        "dividends" : industry.dividends,
        "change1d": industry.change1d,
        "marketCap": industry.marketCap,
        "volume": industry.volume,
        "sector": industry.sector_name
        }
    return industry_dict

