from flask import Flask
from flask import Blueprint
from create_db import app, db, Sectors, Industries, Companies, create_sectors, create_industries, create_companies
from companies import companies_blueprint
from industries import industries_blueprint
from sectors import sectors_blueprint

#app = Flask(__name__, static_url_path='', static_folder='frontend/public')

app.register_blueprint(companies_blueprint, url_prefix="/companies")
app.register_blueprint(industries_blueprint, url_prefix="/industries")
app.register_blueprint(sectors_blueprint, url_prefix="/sectors")


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
