import './index.css';

import React from 'react';
import ReactDOM from 'react-dom/client';

import {HashRouter, Route, Routes } from 'react-router-dom';

import Navbar from './components/Navbar';
import HomePage from './components/HomePage';
import AboutPage from './components/about/AboutPage';
import CompaniesPage from './components/companies/CompaniesPage';
import Company from './components/companies/Company';
import IndustriesPage from './components/industries/IndustriesPage';
import Industry from './components/industries/Industry';
import SectorsPage from './components/sectors/SectorsPage';
import Sector from './components/sectors/Sector';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <HashRouter>
      <div className="min-h-screen flex flex-col gap-24 pb-24 items-center">
        <Navbar />
        <div className="flex min-h-screen flex-col max-w-[90%] w-[80rem]">
          <Routes>
            <Route path="/" element={<HomePage />} />

            <Route path="/about" element={<AboutPage />} />

            <Route path="/companies" element={<CompaniesPage />} />
            <Route path="/companies/:slug" element={<Company />}/>

            <Route path="/industries" element={<IndustriesPage />} />
            <Route path="/industries/:slug" element={<Industry />}/>

            <Route path="/sectors" element={<SectorsPage />} />
            <Route path="/sectors/:slug" element={<Sector />}/>

          </Routes>
        </div>
      </div>
    </HashRouter>
  </React.StrictMode>
);