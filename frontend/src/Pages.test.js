import { render, screen, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";

import CompaniesPage from "./components/companies/CompaniesPage.jsx";
import Company from "./components/companies/Company.jsx";

import IndustriesPage from "./components/industries/IndustriesPage.jsx";
import Industry from "./components/industries/Industry.jsx";

import SectorsPage from "./components/sectors/SectorsPage.jsx";
import Sector from "./components/sectors/Sector.jsx";

describe("Pages listing all companies, industries, and sectors", () => {
  afterEach(() => {
    cleanup();
  });

  test("Companies page is rendered", () => {
    render(<CompaniesPage />);
    const page = screen.getByTestId("companies-page");
    expect(page).toBeInTheDocument();
  });

  test("Industries page is rendered", () => {
    render(<IndustriesPage />);
    const page = screen.getByTestId("industries-page");
    expect(page).toBeInTheDocument();
  });

  test("Sectors page is rendered", () => {
    render(<SectorsPage />);
    const page = screen.getByTestId("sectors-page");
    expect(page).toBeInTheDocument();
  });
});

describe("Individual pages for companies, industries, and sectors", () => {
  afterEach(() => {
    cleanup();
  });

  test("Individual company page is rendered", () => {
    render(<Company />);
    const page = screen.getByTestId("company-page");
    expect(page).toBeInTheDocument();
  });

  // test("Individual industry page is rendered", () => {
  //   render(<Industry />);
  //   const page = screen.getByTestId("industry-page");
  //   expect(page).toBeInTheDocument();
  // });

  // test("Individual sector page is rendered", () => {
  //   render(<Sector />);
  //   const page = screen.getByTestId("sector-page");
  //   expect(page).toBeInTheDocument();
  // });
});
