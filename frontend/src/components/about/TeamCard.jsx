import axios from 'axios';
import {useState,useEffect, useRef} from 'react';

const TeamCard = ({ name, img, focus, bio, stats, id, gName }) => {

  const [issues, setIssues] = useState(0);
  const commits = useRef(0);
  const [isLoaded,setLoaded] = useState(false);

  let issuesURL = "https://gitlab.com/api/v4/projects/42800534/issues_statistics?scope=all&author_id=" + id;
  let buffer = 0;
  useEffect(() => {

    let page = 1;
    let commitURL = "https://gitlab.com/api/v4/projects/42800534/repository/commits?per_page=100&page=" + page;

    (async () => {
      
      // console.log("debug2 " + name);
      buffer = 0;
      do {
        await axios.get(commitURL).then((response) => {
          buffer = response.data.length;
          // console.log("----------------------------------");
          // console.log(response.data[0]["committer_name"]);
          // console.log(response.data[10]["committer_name"]);
          // console.log(response.data.length + " | " + gName);
          // console.log("----------------------------------");
          for(let i = 0; i < response.data.length; i++){
            if (response.data[i]["author_email"] == gName){
              //console.log("name: "+gName+" | commit no: "+i);
              commits.current = commits.current+1;
            }
          }
          // console.log(gName + " | " + commits.current);
          page += 1;
          commitURL = "https://gitlab.com/api/v4/projects/42800534/repository/commits?per_page=100&page=" + page;
        });
          
      }
      while(buffer == 100);
      setLoaded(true);
    })();

    axios.get(issuesURL).then((response) => {
      setIssues(response.data.statistics.counts.all);
    });
    
  },[isLoaded]);


  return (
    <div className="bg-white w-[20rem] flex flex-col items-center border border-black hover:border-blue-600 shadow hover:shadow-xl rounded-2xl">
      <img
        src={img}
        alt="portrait"
        className="w-full aspect-square object-cover rounded-t-xl"
      />
      <div className="h-full flex flex-col items-center py-6 gap-4">
        <div>
          <h3 className="text-2xl">{name}</h3>
          <label className="text-lg">{focus}</label>
        </div>
        <div className="flex gap-4 text-base">
          <label>Commits: {isLoaded ? commits.current : "---"}</label>
          <label>Issues: {issues}</label>
          <label>Tests: {stats.tests}</label>
        </div>
        <p className="text-base w-3/4">{bio}</p>
      </div>
    </div>
  );
};

export default TeamCard;
