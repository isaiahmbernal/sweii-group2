const LinkCard = ({ img, name, info, link }) => {
  return (
    <a href={link}>
      <button className="bg-white w-[18rem] h-[18rem] flex flex-col justify-center items-center gap-4 p-8 border border-black hover:border-blue-600 shadow hover:shadow-xl rounded-2xl">
        <img src={img} alt="tool's logo" className="w-1/3 aspect-square object-cover" />
        <h3 className="text-2xl">{name}</h3>
        <p className="text-base">{info}</p>
      </button>
    </a>
  );
};

export default LinkCard;
