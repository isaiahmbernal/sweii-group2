import LinkCard from "./LinkCard";
import TeamCard from "./TeamCard";
import TotalStats from "./TotalStats";

const AboutPage = () => {
  const team = [
    {
      name: "Shayan Far",
      img: "/images/shayan.jpeg",
      focus: "Back-End",
      bio: "I'm a fourth year ME major at UT Austin and I'm from the Houston area. I expect to graduate in Spring 2023. I enjoy playing video games and watching movies.",
      stats: { commits: 37, issues: 20, tests: 10 },
      id: 10671672,
      gName: "shayanfar01@gmail.com"
    },
    {
      name: "Isaiah M. Bernal",
      img: "/images/isaiah.jpeg",
      focus: "Front-End",
      bio: "I'm a fourth year AET major at UT Austin also studying computer science and Japanese. I expect to graduate December 2023. I enjoy hanging out with Shayan.",
      stats: { commits: 83, issues: 41, tests: 4 },
      id: 10655076,
      gName: "isaiahmbernal@gmail.com"
    },
    {
      name: "Ashwin Hingwe",
      img: "/images/ashwin.jpg",
      focus: "Front-End",
      bio: "I'm a Mechanical Engineering Major at UT Austin. I'm passionate about medical robotics and manufacturing, and am always looking for new projects to gain experience.",
      stats: { commits: 7, issues: 10, tests: 0 },
      id: 12355236,
      gName: "ashwinh@utexas.edu"
    },
    {
      name: "Jingsi Zhou",
      img: "/images/jingsi.jpg",
      focus: "Back-End",
      bio: "I'm a fourth year Mechanical Engineering major at UT Austin. I enjoy baking, hiking, and bouldering in my free time. I look forward to working in embedded systems software engineering after graduation.",
      stats: { commits: 1, issues: 7, tests: 0 },
      id: 10681704,
      gName: "jingsi.zhou@utexas.edu"
    },
  ];

  const tools = [
    {
      img: "/images/react.png",
      name: "React",
      info: "A JavaScript library for building user interfaces",
      link: "https://reactjs.org",
    },
    {
      img: "/images/flask.png",
      name: "Flask",
      info: "A back-end microframework for Python",
      link: "https://flask.palletsprojects.com/en/2.0.x/",
    },
    {
      img: "/images/googlecloud.png",
      name: "Google Cloud Platform",
      info: "Hosting the application with the Application Engine",
      link: "https://cloud.google.com",
    },
    {
      img: "/images/git.png",
      name: "Google Cloud Platform",
      info: "Used for version control",
      link: "https://git-scm.com",
    },
  ];

  const sources = [
    {
      img: "/images/yahoo.jpg",
      name: "Yahoo! Finance",
      info: "Used for general company data",
      link: "https://finance.yahoo.com/",
    },
    {
      img: "/images/tradingview.png",
      name: "TradingView",
      info: "Used for sector and industry data",
      link: "https://www.tradingview.com/",
    },
    {
      img: "/images/postman.png",
      name: "Postman",
      info: "Used for API testing, click to see our documentation",
      link: "https://documenter.getpostman.com/view/26272023/2s93RWMqP8",
    }
  ];

  const gitlab = [
    {
      img: "/images/gitlab.png",
      name: "GitLab Issue Tracker",
      info: "Link to the GitLab issues",
      link: "https://gitlab.com/isaiahmbernal/sweii-group2/-/issues",
    },
    {
      img: "/images/gitlab.png",
      name: "GitLab Repo",
      info: "Link to the GitLab repo",
      link: "https://gitlab.com/isaiahmbernal/sweii-group2",
    },
    {
      img: "/images/gitlab.png",
      name: "GitLab Wiki",
      info: "Link to the GitLab wiki",
      link: "https://gitlab.com/isaiahmbernal/sweii-group2/-/wikis/home",
    },
    {
      img: "/images/speakerdeck.png",
      name: "Speaker Deck",
      info: "Link to our presentation",
      link: "https://speakerdeck.com/shayanfar/software-engineering-2",
    },
  ];

  const gitstats = {
    tests: 10,
    pythonTests: "/images/UnitTests-Python.jpeg",
    jestTests: "/images/UnitTests-Jest.jpg"
  };

  const renderLinkCards = (sources) => {
    return sources.map((source, index) => (
      <LinkCard
        key={index}
        img={source.img}
        name={source.name}
        info={source.info}
        link={source.link}
      />
    ));
  };

  return (
    <div className="flex flex-col gap-24 items-center">
      <section className="flex flex-col gap-8 text-3xl font-medium text-center">
        <h1>Stonkr Team</h1>
        <div className="flex flex-wrap gap-4 justify-center">
          {team.map((member, index) => (
            <TeamCard
              key={index}
              name={member.name}
              img={member.img}
              focus={member.focus}
              bio={member.bio}
              stats={member.stats}
              id={member.id}
              gName={member.gName}
            />
          ))}
        </div>
      </section>
      <section className="flex flex-col gap-8 text-3xl font-medium text-center">
        <h1>Tools</h1>
        <div className="flex flex-wrap justify-center items-center gap-4">
          {renderLinkCards(tools)}
        </div>
      </section>
      <section className="flex flex-col gap-8 text-3xl font-medium text-center">
        <h1>Data</h1>
        <div className="flex flex-wrap justify-center items-center gap-4">
        {renderLinkCards(sources)}
        </div>
      </section>
      <section className="flex flex-col gap-8 text-3xl font-medium text-center">
        <h1>GitLab / Presentation</h1>
        <div className="flex flex-wrap justify-center items-center gap-4">
          {renderLinkCards(gitlab)}
        </div>
      </section>
      <TotalStats
        tests={gitstats.tests}
        pythonTests ={gitstats.pythonTests}
        jestTests ={gitstats.jestTests}
      />
    </div>
  );
};

export default AboutPage;
