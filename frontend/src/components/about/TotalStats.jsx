import axios from 'axios';
import {useState,useEffect, useRef} from 'react';
//import UnitTests from "../../../public/images/UnitTests.jpeg";


//let page = 1;
//let commitURL = "https://gitlab.com/api/v4/projects/42800534/repository/commits?per_page=100&page=" + page;
const issuesURL = "https://gitlab.com/api/v4/projects/42800534/issues_statistics";
let totalCommits = [];
let buffer = 0;



const TotalStats = ({tests, pythonTests, jestTests}) => {
  const [commits, setCommits] = useState(0);
  const [issues, setIssues] = useState();
  const [showPythonTests, setShowPythonTests] = useState(false);
  const [showJestTests, setShowJestTests] = useState(false);
  const firstRender = useRef(true);
  

  //console.log("debug0");

  useEffect(() => {
    totalCommits = [];
    let page = 1;
    let commitURL = "https://gitlab.com/api/v4/projects/42800534/repository/commits?per_page=100&page=" + page;

  
    //console.log("debug0.5");
    (async () => {
      //console.log("debug1");
      if (firstRender.current){
        firstRender.current = false;
        // console.log("debug2: total");
        buffer = 0;
        do {
          await axios.get(commitURL).then((response) => {
            buffer = response.data.length;
            setCommits(commits + response.data.length);
            totalCommits.push(response.data.length);
            // console.log("page: " + page + " | commits: " + buffer + " | firstRender: " + firstRender.current);
            page += 1;
            commitURL = "https://gitlab.com/api/v4/projects/42800534/repository/commits?per_page=100&page=" + page;
          });
            
        }
        while(buffer == 100);
        //console.log("end value: " + buffer);
        // console.log(totalCommits);
        
      }
    })();

    axios.get(issuesURL).then((response) => {
      setIssues(response.data.statistics.counts.all);
      buffer = response.data;
    });
    buffer = 0;
    
  },[]);


function showPythonHandler(){
  if (!showPythonTests){
    setShowPythonTests(true);
    setShowJestTests(false);
  }else{
    setShowPythonTests(false);
  }
}

function showJestHandler(){
  if (!showJestTests){
    setShowJestTests(true);
    setShowPythonTests(false);
  }else{
    setShowJestTests(false);
  }
}

  return (
    <div>
      <div className="bg-white flex px-3 py-1 gap-4 border border-black shadow">
        <label>Total Commits: {totalCommits.reduce((partialSum, a) => partialSum + a,0)}</label>|
        <label>Total Issues: {issues}</label>|
        <label>Total Tests: 10</label>|
        <button onClick={showPythonHandler}>Run Python Tests</button>|
        <button onClick={showJestHandler}>Run Jest Tests</button>
      </div>
      {showPythonTests && <img src={pythonTests} alt="python unit tests" />}
      {showJestTests && <img src={jestTests} alt="jest unit tests" />}
    </div>
  );
};

export default TotalStats;
