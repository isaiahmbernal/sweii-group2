import { Link } from "react-router-dom";

const HomePage = () => {
  const models = [
    { name: "Companies", link: "/companies", img: "/images/companies.jpeg" },
    { name: "Industries", link: "/industries", img: "/images/industries.jpeg" },
    { name: "Sectors", link: "/sectors", img: "/images/sectors.png" },
  ];

  const renderModels = (models) => {
    return models.map((model, index) => (
      <Link to={model.link} key={index}>
        <button className="bg-white border border-black w-[13rem] h-[13rem] hover:font-medium flex flex-col justify-center items-center gap-4 py-4 shadow rounded-md hover:shadow-xl hover:border-blue-600">
          <label>{model.name}</label>
          <img
            src={model.img}
            alt="model"
            className="aspect-square object-cover w-[50%]"
          />
        </button>
      </Link>
    ));
  };

  return (
    <div className="flex flex-col gap-24 items-center">
      <section className="flex flex-col gap-8 text-3xl font-medium text-center">
        <h1>🎉 Welcome to Stonkr 🎉</h1>
        <h3>💰 The best way to track your investments 💰</h3>
        <h3>🤘 To get started, just choose a topic 🤘</h3>
      </section>
      <section className="flex flex-wrap justify-center gap-4">
        {renderModels(models)}
      </section>
    </div>
  );
};

export default HomePage;
