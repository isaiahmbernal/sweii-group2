import SectorsRow from "./SectorsRow";
import { useEffect, useState } from "react";

const SubSectors = ({ sectorName }) => {
  const [sector, setSector] = useState({});
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetch(process.env.REACT_APP_ROUTE_SECTORS + `${sectorName}`)
      .then((res) => res.json())
      .then((sector) => {
        setSector(sector);
        setLoaded(true);
      });
  }, []);

  // console.log(JSON.stringify(sector))

  const renderSectorList = () => {
    return (
      <SectorsRow
        name={sector.name}
        image={sector.image}
        dividends={sector.dividends}
        change1d={sector.change1d}
        marketCap={sector.marketCap}
        volume={sector.volume}
      ></SectorsRow>
    );
  };

  return (
    <div className="w-full flex flex-col gap-8 items-center">
      <h1 className="text-3xl font-bold">Sector</h1>
      <div className="min-w-[65rem] w-full flex flex-col border-2 border-black rounded-lg shadow-xl">
        <div className="flex p-4">
          <label className="text-left w-[40%]">Sector</label>
          <label className="text-center w-[15%]">Dividends</label>
          <label className="text-center w-[15%]">Change 1D</label>
          <label className="text-center w-[15%]">Market Cap</label>
          <label className="text-center w-[15%]">Volume 1D</label>
        </div>
        {/* <div>{JSON.stringify(sector)}</div> */}
        {isLoaded ? renderSectorList() : "Loading..."}
      </div>
    </div>
  );
};

export default SubSectors;
