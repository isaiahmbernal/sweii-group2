import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import SubCompanies from "../companies/SubCompanies";
import SubIndustries from "../industries/SubIndustries";

const Sector = () => {
  const { slug } = useParams();
  const [singleSector, setSector] = useState({});
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetch(process.env.REACT_APP_ROUTE_SECTORS + `${slug}`).then(
      res => res.json()
    ).then(
      singleSector => {
        setSector(singleSector)
        // console.log(singleSector)
        setLoaded(true);
      }
    )
  }, [slug]);

  let name = slug;
  let imgSrc = name;
  if (imgSrc.includes("/")) {
    const newImgSrc = imgSrc.replace(new RegExp("/", "g"), "-")
    // console.log(newImgSrc);
    imgSrc = newImgSrc;
  }
  // console.log("Remove / src:", imgSrc);

  if (imgSrc.includes(" ")) {
    const newImgSrc = imgSrc.replace(new RegExp(" ", "g"), "")
    // console.log(newImgSrc);
    imgSrc = newImgSrc;
  }
  // console.log("Remove Space src:", imgSrc);

  if (imgSrc.includes(":")) {
    const newImgSrc = imgSrc.replace(new RegExp(":", "g"), "_")
    // console.log(newImgSrc);
    imgSrc = newImgSrc;
  }
  // console.log("Remove : src:", imgSrc);

  if (imgSrc.includes("Advertising")) {
    const newImgSrc = imgSrc.replace(new RegExp("Advertising", "g"), "Bruh")
    // console.log(newImgSrc);
    imgSrc = newImgSrc;
  }
  // console.log("Remove : src:", imgSrc);

  return (
    <div data-testid="sector-page" className="flex flex-col gap-24 items-center">
      <div className="w-full flex flex-col gap-8 items-center">
        <img
          className="w-[5rem] aspect-square object-cover rounded-md shadow-lg"
          src={`./images/${imgSrc}.jpeg`}
        ></img>
        <h1 className="text-3xl font-bold">{slug} Sector</h1>
        <div className="min-w-[50rem] w-full flex flex-col border-2 border-black rounded-lg shadow-xl">
          <div className="flex p-4 border-b-2 border-black">
            <label className="text-center w-[25%]">Dividends</label>
            <label className="text-center w-[25%]">Change 1D</label>
            <label className="text-center w-[25%]">Market Cap</label>
            <label className="text-center w-[25%]">Volume 1D</label>
          </div>
          <div className="flex p-4">
            <label className="text-center w-[25%]">{singleSector.dividends}</label>
            <label className="text-center w-[25%]">{singleSector.change1d}</label>
            <label className="text-center w-[25%]">${singleSector.marketCap}</label>
            <label className="text-center w-[25%]">{singleSector.volume}</label>
          </div>
        </div>
      </div>
      {/* { isLoaded ? <div>{JSON.stringify(singleSector.industries)}</div> : ""} */}
      {isLoaded ? <SubIndustries industryList={singleSector.industries}/> : <p className="w-[15rem] animate-pulse border-black rounded-md border-[.1rem] text-center px-3 py-2">Loading Industries</p> }
      {isLoaded ? <SubCompanies companyList={singleSector.companies}/> : <p className="w-[15rem] animate-pulse border-black rounded-md border-[.1rem] text-center px-3 py-2">Loading Companies</p> }
    </div>
  );
};

export default Sector;
