import { Link } from "react-router-dom";

const SectorsRow = ({
  name,
  image,
  dividends,
  change1d,
  marketCap,
  volume,
}) => {

  let imgSrc = name;
  if (imgSrc.includes("/")) {
    const newImgSrc = imgSrc.replace(new RegExp("/", "g"), "-")
    // console.log(newImgSrc);
    imgSrc = newImgSrc;
  }
  // console.log("Remove / src:", imgSrc);

  if (imgSrc.includes(" ")) {
    const newImgSrc = imgSrc.replace(new RegExp(" ", "g"), "")
    // console.log(newImgSrc);
    imgSrc = newImgSrc;
  }
  // console.log("Remove Space src:", imgSrc);

  if (imgSrc.includes(":")) {
    const newImgSrc = imgSrc.replace(new RegExp(":", "g"), "_")
    // console.log(newImgSrc);
    imgSrc = newImgSrc;
  }
  // console.log("Remove : src:", imgSrc);

  if (imgSrc.includes("Advertising")) {
    const newImgSrc = imgSrc.replace(new RegExp("Advertising", "g"), "Bruh")
    // console.log(newImgSrc);
    imgSrc = newImgSrc;
  }
  // console.log("Remove : src:", imgSrc);

  return (
    <Link to={`/sectors/${name}`}>
      <div className="flex p-4 items-center border-t-2 border-black rounded-b-lg hover:bg-gray-100 cursor-pointer">
        <div className="text-left w-[40%] flex gap-4 items-center">
          <img
            className="w-[10%] aspect-square object-cover rounded"
            src={`./images/${imgSrc}.jpeg`}
            alt="sector"
          ></img>
          <label className="w-[90%]">{name}</label>
        </div>
        <label className="text-center w-[15%]">{dividends}</label>
        <label className="text-center w-[15%]">{change1d}</label>
        <label className="text-center w-[15%]">${marketCap}</label>
        <label className="text-center w-[15%]">{volume}</label>
      </div>
    </Link>
  );
};

export default SectorsRow;
