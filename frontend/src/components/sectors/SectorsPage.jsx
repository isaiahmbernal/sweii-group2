import SectorsRow from "./SectorsRow";
import { useEffect, useState } from "react";

const SectorsPage = () => {
  const [sectors, setSectors] = useState([{name: "Commercial Services",}]);
  const [isLoaded, setLoaded] = useState(false);
  const [viewedSectors, setViewedSectors] = useState({
    start: 0,
    end: 10,
  });
  const [filteredSectors, setFilteredSectors] = useState(sectors);
  const [search, setSearch] = useState("");

  useEffect(() => {
    fetch(process.env.REACT_APP_ROUTE_SECTORS)
      .then((res) => res.json())
      .then((sectors) => {
        setSectors(sectors);
        setLoaded(true);
        // console.log("Fetched Sectors:", sectors);
      });
  }, []);

  useEffect(() => {
    if (search === "") {
      setFilteredSectors(sectors);
      setViewedSectors({ start: 0, end: 10 });
    } else {
      let filtered = sectors.filter((sector) =>
        sector.name.toUpperCase().includes(search)
      );
      // console.log("Filtered Sectors:", filtered);
      setFilteredSectors(filtered);
      setViewedSectors({ start: 0, end: 10});
    }
  }, [isLoaded, search]);

  const pageButtonLogic = (direction) => {
    if (direction === "Prev") {
      if (viewedSectors.start === 0) {
        // console.log("Nope");
        return;
      } else {
        if (viewedSectors.end % 10 !== 0) {
          setViewedSectors({
            start: viewedSectors.start - 10,
            end: viewedSectors.end - (viewedSectors.end % 10),
          });
        } else {
          setViewedSectors({
            start: viewedSectors.start - 10,
            end: viewedSectors.end - 10,
          });
        }
      }
    } else if (direction === "Next") {
      if (viewedSectors.end >= filteredSectors.length) {
        // console.log("Nope");
        return;
      } else {
        setViewedSectors({
          start: viewedSectors.start + 10,
          end: viewedSectors.end + 10,
        });
        return;
      }
    }
  };

  function sortBySelection(sortSelect) {
    const sortType =  sortSelect.target.value;
    // console.log(sortType);
    const copySector = JSON.parse(JSON.stringify(filteredSectors));
    if (sortType == "a-z") {
      copySector.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1))
    } else if (sortType == "z-a") {
      copySector.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? 1 : -1))
    } else if (sortType == "diva") {
      copySector.sort((a, b) => (parseFloat(a.dividends.replace('%', '').replace("\u2014","0")) < parseFloat(b.dividends.replace('%', '').replace("\u2014","0")) ? -1 : 1))
    } else if (sortType == "divd") {
      copySector.sort((a, b) => (parseFloat(a.dividends.replace('%', '').replace("\u2014","0")) < parseFloat(b.dividends.replace('%', '').replace("\u2014","0")) ? 1 : -1))
    } else if (sortType == "cha") {
      copySector.sort((a, b) => (parseFloat(a.change1d.replace('%', '').replace("\u2212","-")) < parseFloat(b.change1d.replace('%', '').replace("\u2212","-")) ? -1 : 1))
    } else if (sortType == "chd") {
      copySector.sort((a, b) => (parseFloat(a.change1d.replace('%', '').replace("\u2212","-")) < parseFloat(b.change1d.replace('%', '').replace("\u2212","-")) ? 1 : -1))
    } else if (sortType == "mca") {
      for (var i = 0; i < copySector.length; ++i) {
        if (copySector[i].marketCap.includes("M")) {
          copySector[i].marketCap = copySector[i].marketCap.replace("M", "")
          copySector[i].marketCap *= 10e6;
        } else if (copySector[i].marketCap.includes("B")) {
          copySector[i].marketCap = copySector[i].marketCap.replace("B", "")
          copySector[i].marketCap *= 10e9;
        } else if (copySector[i].marketCap.includes("T")) {
          copySector[i].marketCap = copySector[i].marketCap.replace("T", "")
          copySector[i].marketCap *= 10e12;
        }
      }
      copySector.sort((a, b) => (a.marketCap < b.marketCap ? -1 : 1))
      for (var i = 0; i < copySector.length; ++i) {
        let oldKey = sectors.find(element => element.name == copySector[i].name)
        copySector[i].marketCap = oldKey.marketCap
      } 
    } else if (sortType == "mcd") {
      for (var i = 0; i < copySector.length; ++i) {
        if (copySector[i].marketCap.includes("M")) {
          copySector[i].marketCap = copySector[i].marketCap.replace("M", "")
          copySector[i].marketCap *= 10e6;
        } else if (copySector[i].marketCap.includes("B")) {
          copySector[i].marketCap = copySector[i].marketCap.replace("B", "")
          copySector[i].marketCap *= 10e9;
        } else if (copySector[i].marketCap.includes("T")) {
          copySector[i].marketCap = copySector[i].marketCap.replace("T", "")
          copySector[i].marketCap *= 10e12;
        }
      }
      copySector.sort((a, b) => (a.marketCap < b.marketCap ? 1 : -1))
      for (var i = 0; i < copySector.length; ++i) {
        let oldKey = sectors.find(element => element.name == copySector[i].name)
        copySector[i].marketCap = oldKey.marketCap
      } 
    } else if (sortType == "vola") {
      for (var i = 0; i < copySector.length; ++i) {
        if (copySector[i].volume.includes("K")) {
          copySector[i].volume = copySector[i].volume.replace("K", "")
          copySector[i].volume *= 10e3;
        } else {
          copySector[i].volume = copySector[i].volume.replace("M", "")
          copySector[i].volume *= 10e6;
        }
      }
      copySector.sort((a, b) => (a.volume < b.volume ? -1 : 1))
      for (var i = 0; i < copySector.length; ++i) {
        let oldKey = sectors.find(element => element.name == copySector[i].name)
        copySector[i].volume = oldKey.volume
      }
    } else {
      for (var i = 0; i < copySector.length; ++i) {
        if (copySector[i].volume.includes("K")) {
          copySector[i].volume = copySector[i].volume.replace("K", "")
          copySector[i].volume *= 10e3;
        } else {
          copySector[i].volume = copySector[i].volume.replace("M", "")
          copySector[i].volume *= 10e6;
        }
      }
      copySector.sort((a, b) => (a.volume < b.volume ? 1 : -1))
      for (var i = 0; i < copySector.length; ++i) {
        let oldKey = sectors.find(element => element.name == copySector[i].name)
        copySector[i].volume = oldKey.volume
      }
    }
    setFilteredSectors(copySector);
    return;
  };

  const renderSectorList = (sectors) => {
    let currSectors = filteredSectors.slice(
      viewedSectors.start,
      viewedSectors.end
    );

    return currSectors.map((sector, index) => (
      <SectorsRow
        key={index}
        name={sector.name}
        image={sector.image}
        dividends={sector.dividends}
        change1d={sector.change1d}
        marketCap={sector.marketCap}
        volume={sector.volume}
      />
    ));
  };

  return (
    <div data-testid="sectors-page" className="flex flex-col gap-8 items-center">
      <h1 className="text-3xl font-bold">Sectors</h1>
      <div className="flex w-full justify-between">
        <div className="border-2 border-black rounded px-2 py-1">
          Sort by: 
          <select onChange={sortBySelection}>
            <option value="a-z"> Alphabetical (A-Z) </option>
            <option value="z-a"> Alphabetical (Z-A) </option>
            <option value="diva"> Dividends - Ascending </option>
            <option value="divd"> Dividends - Descending </option>
            <option value="cha"> Change - Ascending </option>
            <option value="chd"> Change - Descending </option>
            <option value="mca"> Market Cap - Ascending </option>
            <option value="mcd"> Market Cap - Descending </option>
            <option value="vola"> Volume - Ascending </option>
            <option value="volb"> Volume - Descending </option>
          </select>
        </div>
        <div>
          <input
            value={search}
            onChange={(event) => setSearch(event.target.value.toUpperCase())}
            placeholder="Search for sector..."
            className="justify-end border-2 border-black rounded px-2 py-1 "
          ></input>
        </div>
      </div>
      <div className="min-w-[65rem] w-full flex flex-col border-2 border-black rounded-lg shadow-xl">
        <div className="flex p-4">
          <label className="text-left w-[40%]">Sector</label>
          <label className="text-center w-[15%]">Dividends</label>
          <label className="text-center w-[15%]">Change 1D</label>
          <label className="text-center w-[15%]">Market Cap</label>
          <label className="text-center w-[15%]">Volume 1D</label>
        </div>
        {isLoaded ? renderSectorList(sectors) : <p className="animate-pulse bg-gray-100 text-center py-2">Loading</p>}
        <div className="flex justify-center gap-4 p-4 border-t-2 border-black rounded-b bg-gray-100">
          <button
            className="bg-blue-500 hover:bg-blue-600 hover:font-bold rounded text-white p-2"
            onClick={() => pageButtonLogic("Prev")}
          >
            Prev
          </button>

          <button
            className="bg-blue-500 hover:bg-blue-600 hover:font-bold rounded text-white p-2"
            onClick={() => pageButtonLogic("Next")}
          >
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default SectorsPage;
