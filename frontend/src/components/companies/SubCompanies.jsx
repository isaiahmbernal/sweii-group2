import CompaniesRow from "./CompaniesRow";
import { useEffect, useState } from "react";

const SubCompanies = ({ companyList }) => {
  const [companies, setCompanies] = useState({});
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetch(process.env.REACT_APP_ROUTE_COMPANIES)
      .then((res) => res.json())
      .then((companies) => {
        setCompanies(companies);
        // console.log('Companies 1')
        // console.log(companies)
        setLoaded(true);
      });
  }, []);

  const renderCompanyList = () => {
    return companies.map((company, index) =>
      companyList.includes(company.name) ? (
        <CompaniesRow
          key={index}
          ticker={company.ticker}
          name={company.name}
          image={company.image}
          price={company.price}
          marketCap={company.marketCap}
          volume={company.volume}
        ></CompaniesRow>
      ) : (
        ""
      )
    );
  };

  return (
    <div className="w-full flex flex-col gap-8 items-center">
      <h1 className="text-3xl font-bold">Companies</h1>
      <div className="min-w-[65rem] w-full flex flex-col border-2 border-black rounded-lg shadow-xl">
        <div className="flex p-4">
          <label className="text-left w-[40%]">Company</label>
          <label className="text-center w-[15%]">Ticker</label>
          <label className="text-center w-[15%]">Price</label>
          <label className="text-center w-[15%]">Market Cap</label>
          <label className="text-center w-[15%]">Volume 1D</label>
        </div>
        {/* <div>{JSON.stringify(companies)}</div> */}
        {isLoaded ? renderCompanyList() : "Loading..."}
      </div>
    </div>
  );
};

export default SubCompanies;
