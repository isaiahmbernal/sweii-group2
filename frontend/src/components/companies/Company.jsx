import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import SubIndustries from "../industries/SubIndustries";
import SubSectors from "../sectors/SubSectors";

const Company = () => {
  const { slug } = useParams();
  const [singleCompany, setCompany] = useState({});
  const [isLoaded, setLoaded] = useState(false);

  // console.log(process.env.REACT_APP_ROUTE_COMPANIES);

  useEffect(() => {
    fetch(process.env.REACT_APP_ROUTE_COMPANIES + `${slug}`).then(
      res => res.json()
    ).then(
      singleCompany => {
        setCompany(singleCompany)
        // console.log(singleCompany)
        setLoaded(true);
      }
    )
  }, [slug]);


  return (
    <div data-testid="company-page" className="flex flex-col gap-24 items-center">
      <div className="w-full flex flex-col gap-8 items-center">
        <img
          className="w-[5rem] aspect-square object-cover rounded-md shadow-lg"
          src={singleCompany.image}
        ></img>
        <h1 className="text-3xl font-bold">{singleCompany.name}</h1>
        <div className="min-w-[50rem] w-full flex flex-col border-2 border-black rounded-lg shadow-xl">
          <div className="flex p-4 border-b-2 border-black">
            <label className="text-center w-[25%]">Ticker</label>
            <label className="text-center w-[25%]">Price</label>
            <label className="text-center w-[25%]">Market Cap</label>
            <label className="text-center w-[25%]">Volume 1D</label>
          </div>
          <div className="flex p-4">
            <label className="text-center w-[25%]">{slug}</label>
            <label className="text-center w-[25%]">${singleCompany.price}</label>
            <label className="text-center w-[25%]">${singleCompany.marketCap}</label>
            <label className="text-center w-[25%]">{singleCompany.volume}</label>
          </div>
        </div>
      </div>
      {isLoaded ? <SubIndustries industryList={singleCompany.industries}/> : <p className="w-[15rem] animate-pulse border-black rounded-md border-[.1rem] text-center px-3 py-2">Loading Industries</p>}
      {isLoaded ? <SubSectors sectorName={singleCompany.sectors}/> : <p className="w-[15rem] animate-pulse border-black rounded-md border-[.1rem] text-center px-3 py-2">Loading Sectors</p> }
    </div>
  );
};

export default Company;
