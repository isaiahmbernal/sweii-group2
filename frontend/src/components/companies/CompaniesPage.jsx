import CompaniesRow from "./CompaniesRow";
import { useEffect, useState } from "react";

const CompaniesPage = () => {
  const [companies, setCompanies] = useState([{name: "Bruh"}]);
  const [isLoaded, setLoaded] = useState(false);

  const [filteredCompanies, setFilteredCompanies] = useState(companies);
  const [viewedCompanies, setViewedCompanies] = useState({
    start: 0,
    end: 10,
  });
  const [search, setSearch] = useState("");
  const [sortedCompanies, setSortedCompanies] = useState();

  useEffect(() => {
    if (isLoaded) return;
    fetch(process.env.REACT_APP_ROUTE_COMPANIES)
      .then((res) => res.json())
      .then((companies) => {
        setCompanies(companies.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1)));
        setSortedCompanies(companies.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1)));
        setFilteredCompanies(companies.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1)));
        setLoaded(true);
        // console.log("Retrieved Companies:", companies);
      });
  }, [isLoaded]);

  useEffect(() => {
    if (search === "") {
      setFilteredCompanies(sortedCompanies);
      setViewedCompanies({ start: 0, end: 10 });
    } else {
      let filtered = sortedCompanies.filter((company) =>
        company.name.toUpperCase().includes(search)
      );
      // console.log("Filtered Companies:", filtered);
      setFilteredCompanies(filtered);
      setViewedCompanies({ start: 0, end: 10});
    }
  }, [isLoaded, search, sortedCompanies]);

  const renderCompanyList = () => {
    let currCompanies = filteredCompanies.slice(
      viewedCompanies.start,
      viewedCompanies.end
    );

    return currCompanies.map((company, index) => (
      <CompaniesRow
        key={index}
        name={company.name}
        image={company.image}
        ticker={company.ticker} 
        price={company.price}
        marketCap={company.marketCap}
        volume={company.volume}
      />
    ));
  };

  function sortBySelection(sortSelect) {
    const sortType =  sortSelect;
    const copyCompany = JSON.parse(JSON.stringify(companies));

    // console.log(sortType);
    if (sortType == "a-z") {
      copyCompany.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1))
    } else if (sortType == "z-a") {
      copyCompany.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? 1 : -1))
    } else if (sortType == "pra") {
      copyCompany.sort((a, b) => (parseFloat(a.price) < parseFloat(b.price) ? -1 : 1))
    } else if (sortType == "prd") {
      copyCompany.sort((a, b) => (parseFloat(a.price) < parseFloat(b.price) ? 1 : -1))
    } else if (sortType == "mca") {
      for (var i = 0; i < copyCompany.length; ++i) {
        if (copyCompany[i].marketCap.includes("M")) {
          copyCompany[i].marketCap = copyCompany[i].marketCap.replace("M", "").replace('USD','') 
          copyCompany[i].marketCap *= 10e6
        } else if (copyCompany[i].marketCap.includes("B")) {
          copyCompany[i].marketCap = copyCompany[i].marketCap.replace("B", "").replace('USD','') 
          copyCompany[i].marketCap *= 10e9
        } else if (copyCompany[i].marketCap.includes("T")) {
          copyCompany[i].marketCap = copyCompany[i].marketCap.replace("T", "").replace('USD','') 
          copyCompany[i].marketCap *= 10e12
        } else {
          break;
        }
      }
      copyCompany.sort((a, b) => (parseFloat(a.marketCap) < parseFloat(b.marketCap) ? -1 : 1))
      for (var i = 0; i < copyCompany.length; ++i) {
        let oldKey = companies.find(element => element.ticker == copyCompany[i].ticker)
        copyCompany[i].marketCap = oldKey.marketCap
      } 
    } else if (sortType == "mcd") {
      for (var i = 0; i < copyCompany.length; ++i) {
        if (copyCompany[i].marketCap.includes("M")) {
          copyCompany[i].marketCap = copyCompany[i].marketCap.replace("M", "").replace('USD','') 
          copyCompany[i].marketCap *= 10e6
        } else if (copyCompany[i].marketCap.includes("B")) {
          copyCompany[i].marketCap = copyCompany[i].marketCap.replace("B", "").replace('USD','') 
          copyCompany[i].marketCap *= 10e9
        } else if (copyCompany[i].marketCap.includes("T")) {
          copyCompany[i].marketCap = copyCompany[i].marketCap.replace("T", "").replace('USD','') 
          copyCompany[i].marketCap *= 10e12
        } else {
          break;
        }
      }
      copyCompany.sort((a, b) => (parseFloat(a.marketCap) < parseFloat(b.marketCap) ? 1 : -1))
      for (var i = 0; i < copyCompany.length; ++i) {
        let oldKey = companies.find(element => element.ticker == copyCompany[i].ticker)
        copyCompany[i].marketCap = String(oldKey.marketCap);
      }
    } else if (sortType == "vola") {
      copyCompany.sort((a, b) => (parseFloat(a.volume) < parseFloat(b.volume) ? -1 : 1))
    } else {
      copyCompany.sort((a, b) => (parseFloat(a.volume) < parseFloat(b.volume) ? 1 : -1))
    }
    setSortedCompanies(copyCompany);
    return;
  };

  const pageButtonLogic = (direction) => {
    if (direction === "Prev") {
      if (viewedCompanies.start === 0) {
        // console.log("Nope");
        return;
      } else {
        if (viewedCompanies.end % 10 !== 0) {
          setViewedCompanies({
            start: viewedCompanies.start - 10,
            end: viewedCompanies.end - (viewedCompanies.end % 10),
          });
        } else {
          setViewedCompanies({
            start: viewedCompanies.start - 10,
            end: viewedCompanies.end - 10,
          });
        }
      }
    } else if (direction === "Next") {
      if (viewedCompanies.end >= filteredCompanies.length) {
        // console.log("Nope");
        return;
      } else {
        setViewedCompanies({
          start: viewedCompanies.start + 10,
          end: viewedCompanies.end + 10,
        });
        return;
      }
    }
  };

  return (
    <div data-testid="companies-page" className="flex flex-col gap-8 items-center">
      <h1 className="text-3xl font-bold">Companies</h1>
      <div className="flex w-full justify-between">
        <div className="border-2 border-black rounded px-2 py-1">
          Sort by: 
          <select onChange={e => sortBySelection(e.target.value)}>
            <option value="a-z"> Alphabetical (A-Z) </option>
            <option value="z-a"> Alphabetical (Z-A) </option>
            <option value="pra"> Price - Ascending </option>
            <option value="prd"> Price - Descending </option>
            <option value="mca"> Market Cap - Ascending </option>
            <option value="mcd"> Market Cap - Descending </option>
            <option value="vola"> Volume - Ascending </option>
            <option value="volb"> Volume - Descending </option>
          </select>
        </div>
        <div>
          <input
            value={search}
            onChange={(event) => setSearch(event.target.value.toUpperCase())}
            placeholder="Search for company..."
            className="justify-end border-2 border-black rounded px-2 py-1 "
          ></input>
        </div>
      </div>
      <div className="min-w-[50rem] w-full flex flex-col border-2 border-black rounded-lg shadow-xl">
        <div className="flex p-4">
          <label className="text-left w-[40%]">Company</label>
          <label className="text-center w-[15%]">Ticker</label>
          <label className="text-center w-[15%]">Price</label>
          <label className="text-center w-[15%]">Market Cap</label>
          <label className="text-center w-[15%]">Volume 1D</label>
        </div>
        {isLoaded ? renderCompanyList() : <p className="animate-pulse bg-gray-100 text-center py-2">Loading</p>}
        <div className="flex justify-center gap-4 p-4 border-t-2 border-black rounded-b bg-gray-100">
          <button
            className="bg-blue-500 hover:bg-blue-600 hover:font-bold rounded text-white p-2"
            onClick={() => pageButtonLogic("Prev")}
          >
            Prev
          </button>

          <button
            className="bg-blue-500 hover:bg-blue-600 hover:font-bold rounded text-white p-2"
            onClick={() => pageButtonLogic("Next")}
          >
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default CompaniesPage;
