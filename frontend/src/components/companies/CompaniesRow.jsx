import { Link } from "react-router-dom";

const CompaniesRow = ({ name, image, ticker, price, marketCap, volume }) => {
  return (
    <Link to={`/companies/${ticker}`}>
      <div className="flex p-4 items-center border-t-2 border-black rounded-b-lg hover:bg-gray-100 cursor-pointer">
        <div className="text-left w-[40%] flex gap-4 items-center">
          <img
            className="w-[10%] aspect-square object-cover rounded"
            src={image}
            alt="company"
          ></img>
          <label className="w-[90%]">{name}</label>
        </div>
        <label className="text-center w-[15%]">{ticker}</label>
        <label className="text-center w-[15%]">${price}</label>
        <label className="text-center w-[15%]">${marketCap}</label>
        <label className="text-center w-[15%]">{volume}</label>
      </div>
    </Link>
  );
};

export default CompaniesRow;
