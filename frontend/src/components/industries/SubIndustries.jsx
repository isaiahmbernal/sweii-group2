import IndustriesRow from "./IndustriesRow";
import { useEffect, useState } from "react";

const SubIndustries = ({ industryList }) => {
  const [industries, setIndustries] = useState({});
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetch(process.env.REACT_APP_ROUTE_INDUSTRIES)
      .then((res) => res.json())
      .then((industries) => {
        setIndustries(industries);
        setLoaded(true);
      });
  }, []);

  // console.log(industryList);

  const renderIndustryList = () => {
    return industries.map((industry, index) =>
      industryList.includes(industry.name) ? (
        <IndustriesRow
          key={index}
          name={industry.name}
          image={industry.image}
          dividends={industry.dividends}
          change1d={industry.change1d}
          marketCap={industry.marketCap}
          volume={industry.volume}
        ></IndustriesRow>
      ) : (
        ""
      )
    );
  };

  return (
    <div className="w-full flex flex-col gap-8 items-center">
      <h1 className="text-3xl font-bold">Industries</h1>
      <div className="min-w-[65rem] w-full flex flex-col border-2 border-black rounded-lg shadow-xl">
        <div className="flex p-4">
          <label className="text-left w-[40%]">Industry</label>
          <label className="text-center w-[15%]">Dividends</label>
          <label className="text-center w-[15%]">Change 1D</label>
          <label className="text-center w-[15%]">Market Cap</label>
          <label className="text-center w-[15%]">Volume 1D</label>
        </div>
        {/* { isLoaded ? <div>{JSON.stringify(industries)}</div> : ""} */}
        {isLoaded ? renderIndustryList() : "Loading..."}
      </div>
    </div>
  );
};

export default SubIndustries;
