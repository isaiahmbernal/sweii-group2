import IndustriesRow from "./IndustriesRow";
import { useEffect, useState } from "react";

const IndustriesPage = () => {
  const [industries, setIndustries] = useState([{name: "Advertising/Marketing Services"}]);
  const [isLoaded, setLoaded] = useState(false);
  const [viewedIndustries, setViewedIndustries] = useState({
    start: 0,
    end: 10,
  });
  const [filteredIndustries, setFilteredIndustries] = useState(industries);
  const [search, setSearch] = useState("");

  useEffect(() => {
    fetch(process.env.REACT_APP_ROUTE_INDUSTRIES)
      .then((res) => res.json())
      .then((industries) => {
        setIndustries(industries);
        setLoaded(true);
        // console.log("Fetched Industries:", industries);
      });
  }, [isLoaded]);

  useEffect(() => {
    if (search === "") {
      setFilteredIndustries(industries);
      setViewedIndustries({ start: 0, end: 10 });
    } else {
      let filtered = industries.filter((industry) =>
        industry.name.toUpperCase().includes(search)
      );
      // console.log("Filtered Industries:", filtered);
      setFilteredIndustries(filtered);
      setViewedIndustries({ start: 0, end: 10});
    }
  }, [isLoaded, search]);

  const pageButtonLogic = (direction) => {
    if (direction === "Prev") {
      if (viewedIndustries.start === 0) {
        // console.log("Nope");
        return;
      } else {
        if (viewedIndustries.end % 10 !== 0) {
          setViewedIndustries({
            start: viewedIndustries.start - 10,
            end: viewedIndustries.end - (viewedIndustries.end % 10),
          });
        } else {
          setViewedIndustries({
          start: viewedIndustries.start - 10,
          end: viewedIndustries.end - 10,
        });
        }
      }
    } else if (direction === "Next") {
      if (viewedIndustries.end >= filteredIndustries.length) {
        // console.log("Nope");
        return;
      } else {
        setViewedIndustries({
          start: viewedIndustries.start + 10,
          end: viewedIndustries.end + 10,
        });
        return;
      }
    }
  };

  function sortBySelection(sortSelect) {
    const sortType =  sortSelect.target.value;
    // console.log(sortType);
    const copyIndustry = JSON.parse(JSON.stringify(filteredIndustries));
    if (sortType == "a-z") {
      copyIndustry.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1))
    } else if (sortType == "z-a") {
      copyIndustry.sort((a, b) => (a.name.toUpperCase() < b.name.toUpperCase() ? 1 : -1))
    } else if (sortType == "diva") {
      copyIndustry.sort((a, b) => (parseFloat(a.dividends.replace('%', '').replace("\u2014","0")) < parseFloat(b.dividends.replace('%', '').replace("\u2014","0")) ? -1 : 1))
    } else if (sortType == "divd") {
      copyIndustry.sort((a, b) => (parseFloat(a.dividends.replace('%', '').replace("\u2014","0")) < parseFloat(b.dividends.replace('%', '').replace("\u2014","0")) ? 1 : -1))
    } else if (sortType == "cha") {
      copyIndustry.sort((a, b) => (parseFloat(a.change1d.replace('%', '').replace("\u2212","-")) < parseFloat(b.change1d.replace('%', '').replace("\u2212","-")) ? -1 : 1))
    } else if (sortType == "chd") {
      copyIndustry.sort((a, b) => (parseFloat(a.change1d.replace('%', '').replace("\u2212","-")) < parseFloat(b.change1d.replace('%', '').replace("\u2212","-")) ? 1 : -1))
    } else if (sortType == "mca") {
      for (var i = 0; i < copyIndustry.length; ++i) {
        if (copyIndustry[i].marketCap.includes("M")) {
          copyIndustry[i].marketCap = copyIndustry[i].marketCap.replace("M", "")
          copyIndustry[i].marketCap *= 10e6;
        } else if (copyIndustry[i].marketCap.includes("B")) {
          copyIndustry[i].marketCap = copyIndustry[i].marketCap.replace("B", "")
          copyIndustry[i].marketCap *= 10e9;
        } else if (copyIndustry[i].marketCap.includes("T")) {
          copyIndustry[i].marketCap = copyIndustry[i].marketCap.replace("T", "")
          copyIndustry[i].marketCap *= 10e12;
        }
      }
      copyIndustry.sort((a, b) => (a.marketCap < b.marketCap ? -1 : 1))
      for (var i = 0; i < copyIndustry.length; ++i) {
        let oldKey = industries.find(element => element.name == copyIndustry[i].name)
        copyIndustry[i].marketCap = oldKey.marketCap
      } 
    } else if (sortType == "mcd") {
      for (var i = 0; i < copyIndustry.length; ++i) {
        if (copyIndustry[i].marketCap.includes("M")) {
          copyIndustry[i].marketCap = copyIndustry[i].marketCap.replace("M", "")
          copyIndustry[i].marketCap *= 10e6;
        } else if (copyIndustry[i].marketCap.includes("B")) {
          copyIndustry[i].marketCap = copyIndustry[i].marketCap.replace("B", "")
          copyIndustry[i].marketCap *= 10e9;
        } else if (copyIndustry[i].marketCap.includes("T")) {
          copyIndustry[i].marketCap = copyIndustry[i].marketCap.replace("T", "")
          copyIndustry[i].marketCap *= 10e12;
        }
      }
      copyIndustry.sort((a, b) => (a.marketCap < b.marketCap ? 1 : -1))
      for (var i = 0; i < copyIndustry.length; ++i) {
        let oldKey = industries.find(element => element.name == copyIndustry[i].name)
        copyIndustry[i].marketCap = oldKey.marketCap
      } 
    } else if (sortType == "vola") {
      for (var i = 0; i < copyIndustry.length; ++i) {
        if (copyIndustry[i].volume.includes("K")) {
          copyIndustry[i].volume = copyIndustry[i].volume.replace("K", "")
          copyIndustry[i].volume *= 10e3;
        } else {
          copyIndustry[i].volume = copyIndustry[i].volume.replace("M", "")
          copyIndustry[i].volume *= 10e6;
        }
      }
      copyIndustry.sort((a, b) => (a.volume < b.volume ? -1 : 1))
      for (var i = 0; i < copyIndustry.length; ++i) {
        let oldKey = industries.find(element => element.name == copyIndustry[i].name)
        copyIndustry[i].volume = oldKey.volume
      }
    } else {
      for (var i = 0; i < copyIndustry.length; ++i) {
        if (copyIndustry[i].volume.includes("K")) {
          copyIndustry[i].volume = copyIndustry[i].volume.replace("K", "")
          copyIndustry[i].volume *= 10e3;
        } else {
          copyIndustry[i].volume = copyIndustry[i].volume.replace("M", "")
          copyIndustry[i].volume *= 10e6;
        }
      }
      copyIndustry.sort((a, b) => (a.volume < b.volume ? 1 : -1))
      for (var i = 0; i < copyIndustry.length; ++i) {
        let oldKey = industries.find(element => element.name == copyIndustry[i].name)
        copyIndustry[i].volume = oldKey.volume
      }
    }
    setFilteredIndustries(copyIndustry);
    return;
  };

  const renderIndustryList = () => {
    let currIndustries = filteredIndustries.slice(
      viewedIndustries.start,
      viewedIndustries.end
    );

    return currIndustries.map((industry, index) => (
      <IndustriesRow
        key={index}
        name={industry.name}
        image={industry.image}
        dividends={industry.dividends}
        change1d={industry.change1d}
        marketCap={industry.marketCap}
        volume={industry.volume}
      />
    ));
  };

  return (
    <div data-testid="industries-page" className="flex flex-col gap-8 items-center">
      <h1 className="text-3xl font-bold">Industries</h1>
      <div className="flex w-full justify-between">
        <div className="border-2 border-black rounded px-2 py-1">
          Sort by: 
          <select onChange={sortBySelection}>
            <option value="a-z"> Alphabetical (A-Z) </option>
            <option value="z-a"> Alphabetical (Z-A) </option>
            <option value="diva"> Dividends - Ascending </option>
            <option value="divd"> Dividends - Descending </option>
            <option value="cha"> Change - Ascending </option>
            <option value="chd"> Change - Descending </option>
            <option value="mca"> Market Cap - Ascending </option>
            <option value="mcd"> Market Cap - Descending </option>
            <option value="vola"> Volume - Ascending </option>
            <option value="volb"> Volume - Descending </option>
          </select>
        </div>
        <div>
          <input
            value={search}
            onChange={(event) => setSearch(event.target.value.toUpperCase())}
            placeholder="Search for industry..."
            className="justify-end border-2 border-black rounded px-2 py-1 "
          ></input>
        </div>
      </div>
      <div className="min-w-[65rem] w-full flex flex-col border-2 border-black rounded-lg shadow-xl">
        <div className="flex p-4">
          <label className="text-left w-[40%]">Industry</label>
          <label className="text-center w-[15%]">Dividends</label>
          <label className="text-center w-[15%]">Change 1D</label>
          <label className="text-center w-[15%]">Market Cap</label>
          <label className="text-center w-[15%]">Volume 1D</label>
        </div>
        {isLoaded ? renderIndustryList() : <p className="animate-pulse bg-gray-100 text-center py-2">Loading</p>}
        <div className="flex justify-center gap-4 p-4 border-t-2 border-black rounded-b bg-gray-100">
          <button
            className="bg-blue-500 hover:bg-blue-600 hover:font-bold rounded text-white p-2"
            onClick={() => pageButtonLogic("Prev")}
          >
            Prev
          </button>

          <button
            className="bg-blue-500 hover:bg-blue-600 hover:font-bold rounded text-white p-2"
            onClick={() => pageButtonLogic("Next")}
          >
            Next
          </button>
        </div>
      </div>
    </div>
  );
};

export default IndustriesPage;
