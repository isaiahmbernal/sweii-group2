import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="xs:sticky xs:top-0 w-screen bg-blue-700 text-white flex justify-between px-6 py-4">
      <Link to="/"><h1 className="font-bold hover:font-extrabold text-2xl">Stonkr</h1></Link>
      <div className="flex flex-col xs:flex-row gap-4 items-center">
        <Link className="hover:font-medium" to="/companies">Companies</Link>
        <Link className="hover:font-medium" to="/industries">Industries</Link>
        <Link className="hover:font-medium" to="/sectors">Sectors</Link>
        <Link className="hover:font-medium" to="/about">About Us</Link>
      </div>
    </nav>
  );
};

export default Navbar;
