from flask import Blueprint, jsonify
from models import Sectors


sectors_blueprint = Blueprint("sectors_blueprint", __name__,)   


@sectors_blueprint.route('/')
def sectors():
    sectors = Sectors.query.all()
    sectors_list = []
    for sector in sectors:
        sector_dict = make_dictionarysector(sector)
        sectors_list.append(sector_dict)
    return jsonify(sectors_list)

@sectors_blueprint.route('/<slug>')
def sector(slug):
    sector = Sectors.query.get(slug)
    print(slug)
    initial_JSON = make_dictionarysector(sector)
    companies = sector.sector_companies
    industries = sector.sector_industries
    company_list = []
    industry_list = []

    for company in companies:
        company_list.append(company.name)

    for industry in industries:
        industry_list.append(industry.name)
    
    initial_JSON.update({"companies": company_list})
    initial_JSON.update({"industries": industry_list})

    return jsonify(initial_JSON)


def make_dictionarysector(sector):
    sector_dict = {
        "name": sector.name,
        "image": sector.image,
        "dividends" : sector.dividends,
        "change1d": sector.change1d,
        "marketCap": sector.marketCap,
        "volume": sector.volume,
        }
    return sector_dict

