import pandas as pd
import json

file = pd.read_csv('NASDAQ.csv', header=None) # can also index sheet by name or fetch all sheets
allTickers = file.values.T[0].tolist()
allNames = file.values.T[1].tolist()

finalList = []
for position in range(0, len(allTickers)):
    finalList.append({"ticker":allTickers[position], "name":allNames[position]})

print(finalList)

with open('JSON_NASDAQ_Tickers.json', 'w') as fp:
    json.dump(finalList, fp)